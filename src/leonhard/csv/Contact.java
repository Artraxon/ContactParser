package leonhard.csv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by TheCo on 04.03.2017.
 */
public class Contact {

public HashMap<String, String> information;
public String source;
private static ArrayList<String> fields = new ArrayList<String>(){{
    add("name");
    add("primaryName");
    add("city");
    add("email");
    add("fon");
    add("mobile");
    add("adress");
    add("company");
    add("plz");
    add("job");
}};

    public Contact(/*String name, String primaryName, String city, String email, String fon, String mobile, String adress, String company, int plz*/) {
        information = new HashMap<>();
        /*information.put("name", name);
        information.put("primaryName", primaryName);
        information.put("city", city);
        information.put("email", email);
        information.put("fon", fon);
        information.put("mobile", mobile);
        information.put("adress", adress);
        information.put("company", company);
        information.put("plz", Integer.toString(plz));*/
    }

    public String[] toStringArray(){
        String[] data = new String[fields.size()];
        ArrayList<String> listdata = new ArrayList<>(information.values());
        final int[] i = {0};
        fields.forEach(s -> {
            data[i[0]] = information.get(s);
            if(data[i[0]] == null){
                data[i[0]] = "";
            }
            i[0]++;
        });
        return data;
    }
    public static String[] getFields(){
        return fields.toArray(new String[]{});
    }
}
