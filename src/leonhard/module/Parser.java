package leonhard.module;




import leonhard.csv.Contact;
import leonhard.terminal.Terminal;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.print.Doc;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by TheCo on 04.03.2017.
 */
public class Parser {

    public static HashMap<String, Function<String, HashMap<Document, String>>> metaParser = new HashMap<>();
    public String nameSelector;
    public String primaryNameSelector;
    public String citySelector;
    public String emailSelector;
    public String fonSelector;
    public String mobileSelector;
    public String adressSelector;
    public String plzSelector;

    public Consumer<Contact> postFetchEdit;

    public HashMap<String, String> selectors;

    public Parser(HashMap<String, String> selectors, Consumer<Contact> postFetchEdit){
        this.selectors = selectors;
        this.postFetchEdit = postFetchEdit;
    }

    public ArrayList<Contact> parse(String url) {
        this.selectors = selectors;
        Document doc = null;

        ArrayList<Contact> contacts = new ArrayList<>();
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            try {
                Process curl =  Runtime.getRuntime().exec("curl " + url);
                BufferedReader reader = new BufferedReader(new InputStreamReader(curl.getInputStream()));
                String line = null;
                String result = "";
                while((line = reader.readLine()) != null){
                    result += line;
                }
                doc = Jsoup.parse(result);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        ExecutorService executorService = Executors.newCachedThreadPool();
        getAllDocuments(doc, url).forEach((document, s) -> {

            executorService.submit(() -> {
                Contact current = new Contact();
                selectors.forEach((name, selector) -> {
                    current.information.put(name, document.select(selector).text());
                });
                current.source = s;
                postFetchEdit.accept(current);
                System.out.println("Erfolgreich " + current.information.get("name") + " gefetcht");
                contacts.add(current);
            });
        });
        executorService.shutdown();
        try {
            executorService.awaitTermination(20, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return contacts;


    }
    public HashMap<Document, String> getAllDocuments(Document hubdoc, String url){
        HashMap<Document, String> result = new HashMap<>();
        if(metaParser.entrySet().stream().anyMatch(stringFunctionEntry -> url.contains(stringFunctionEntry.getKey()))){
            result.putAll(metaParser.entrySet()
                    .stream()
                    .filter(stringFunctionEntry ->url.contains(stringFunctionEntry.getKey())).findFirst().get().getValue().apply(url));
        }else {
            result.put(hubdoc, url);
        }
        return result;
    }

    public static Document getDocument(String url){

        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            try {
                Process curl =  Runtime.getRuntime().exec("curl " + url);
                BufferedReader reader = new BufferedReader(new InputStreamReader(curl.getInputStream()));
                String line = null;
                String result = "";
                while((line = reader.readLine()) != null){
                    result += line;
                }
                doc = Jsoup.parse(result);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return doc;
    }

    public static void initMetaParser() {
        metaParser.put("www.dastelefonbuch.de/Suche", s -> {
            HashMap<Document, String> contained = new HashMap<Document, String>();
            ArrayList<Elements> entrycontainer = new ArrayList<Elements>(){{add(getDocument(s).select("#entrycontainer"));}};

            for (int i = 2; !entrycontainer.get(i - 2).select("a.name").isEmpty() ; i++) {
                if(!(getDocument(s + "/" + i).select("#entrycontainer").size() == 0)){
                    entrycontainer.add(getDocument(s + "/" + i).select("#entrycontainer"));
                }
                System.out.println(s + "/" + i);
            }

            entrycontainer.forEach(currentEntryContainer -> currentEntryContainer.get(0).select("a.name").forEach(element -> {
                if (element.hasAttr("href")) {
                    contained.put(getDocument(element.attr("href")), element.attr("href"));
                    System.out.println("Link gefunden: " + element.attr("href"));
                }

            }));

            return contained;
        });
    }

}
