package leonhard.terminal;

import com.opencsv.CSVWriter;
import leonhard.csv.Contact;
import leonhard.module.Parser;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.nodes.Document;

import java.io.*;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by TheCo on 04.03.2017.
 */
public class Terminal {
    public static Terminal instance;
    public HashMap<String, Parser> parsers;
    public CSVWriter writer;
    public static ExecutorService threadPoolExecutor;
    public static void main(String[] args){
        try {
            instance = new Terminal();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Terminal() throws IOException {
        Scanner scn = new Scanner(System.in);
        parsers = new HashMap<>();
        threadPoolExecutor = Executors.newCachedThreadPool();
        prepareCSVFile();
        initParsers();


        Files.lines(FileSystems.getDefault().getPath("input.txt")).forEach(s -> {
            System.out.println("Parse...");
            String baseURL = s.split("//")[1].split("/")[0];
            parsers.entrySet().stream().filter(stringParserEntry -> s.contains(stringParserEntry.getKey())).findFirst().get().getValue().parse(s).forEach(contact -> {
                writer.writeNext(contact.toStringArray());
            });
            System.out.println("Erfolgreich alles in CSV umgegewandelt");

        });
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void prepareCSVFile() {
        File file = new File("data.csv");
        for (int i = 0; file.exists(); i++) {
            file = new File("data" + i + ".csv");
        }
        try {
            writer = new CSVWriter(new FileWriter(file));
            writer.writeNext(Contact.getFields());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initParsers() {
        Parser.initMetaParser();
        parsers.put("www.debeka.de", new Parser(new HashMap<String, String>(){{
            put("name", "#form > table > tbody > tr > td:nth-child(1) > h1");
            put("fon", "#form > div > table > tbody > tr:nth-child(2) > td:nth-child(2)");
            put("mobile", "#form > div > table > tbody > tr:nth-child(4) > td:nth-child(2)");
            put("adress", "#form > div > table > tbody > tr:nth-child(1) > td:nth-child(2) > div");
            put("email", "#cryptMail");
            put("city", "#form > div > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > br");
            put("job", "#form > table > tbody > tr > td:nth-child(1) > h2");
        }}, contact -> {
            nameEdit(contact);
            adressEdit(contact);
            contact.information.put("company", "debeka");
            contact.information.put("email", contact.information.get("name") + "." + contact.information.get("primaryName") + "@debeka.de");
        }));
        parsers.put("www.wuestenrot.de", new Parser(new HashMap<String, String>(){{
            put("name", "body > div.WW_OBJ_area-dc > main > div.WW_MOD_BM_1-0-beraterbuehne > div > div.WW_text-ct-wrap > div > div.WW_col.WW_col-4-dk.WW_contact-ct > div:nth-child(1) > span > h4");
            put("adress", "body > div.WW_OBJ_area-dc > main > div.WW_MOD_BM_1-0-beraterbuehne > div > div.WW_text-ct-wrap > div > div.WW_col.WW_col-4-dk.WW_contact-ct > div:nth-child(1) > address");
            put("fon", "body > div.WW_OBJ_area-dc > main > div.WW_MOD_BM_1-0-beraterbuehne > div > div.WW_text-ct-wrap > div > div.WW_col.WW_col-4-dk.WW_contact-ct > div:nth-child(2) > div > div:nth-child(1) > span:nth-child(2)");
        }}, contact -> {
            nameEdit(contact);
            adressEdit(contact);
            contact.information.put("company", "wüstenrot");
        }));

        parsers.put("dastelefonbuch.de", new Parser(new HashMap<String, String>(){{
            put("name", "#content > div.maininfo.clearfix > h1");
            put("adress", "#content > div.segment.morecontact.clearfix > div > div.primary > address");
            put("email", "#content > div.segment.morecontact.clearfix > div > div.secondary > ul > li:nth-child(1) > a");
            put("job", "#content > div.maininfo.clearfix > div.category > span");
        }}, contact -> {
            try {
                Document doc = null;
                if(Parser.getDocument(contact.source).select("#content > div.maininfo.clearfix > div.number > a").size() != 0) {
                    doc = Parser.getDocument(Parser.getDocument(contact.source).select("#content > div.maininfo.clearfix > div.number > a").attr("href"));
                    contact.information.put("fon", doc.select("#tbr-form-freecall > div.formular > p").get(0).ownText().split(":")[1].trim().replace("%20", " "));
                    contact.information.put("fon",)
                }
                adressEdit(contact);
                if(contact.information.get("adress").contains(", ")){
                    contact.information.put("adress", contact.information.get("adress").split(", ")[0]);
                }
                contact.information.put("email", doc.select("#content > div.segment.morecontact.clearfix > div > div.secondary > ul > li:nth-child(1) > a").get(0).ownText());
                contact.information.put("email", contact.information.get("email").replace("E-Mail", ""));
                contact.information.put("email", contact.information.get("email").replace("senden", ""));
                contact.information.put("email", contact.information.get("email").trim());
            }catch (Exception e){
                System.out.println("wuups");
            }
        }));

    }

    public void nameEdit(Contact contact){

        String name = contact.information.get("name");
        contact.information.put("name", name.split(" ")[1]);
        contact.information.put("primaryName", name.split(" ")[0]);
        contact.information.put("fon", contact.information.get("fon").split(" ")[0] + " -" + contact.information.get("fon").substring(contact.information.get("fon").split(" ")[0].length()));
    }
    public int findFirstDigit(String string){
        char[] charArray = string.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            char c = charArray[i];
            if(Character.isDigit(c)){
                return i;
            }

        }
        return 0;

    }

    public void adressEdit(Contact contact){
        String adress = contact.information.get("adress");
        String[] adresssplit = adress.substring(findFirstDigit(adress)).split(" ");
        if(adresssplit.length <= 2){
            contact.information.put("plz", adresssplit[0]);
            contact.information.put("city", adresssplit[1]);
        }else {
            contact.information.put("adress", adress.split(" ")[0] + " " + adresssplit[0]);
            contact.information.put("plz", adresssplit[1]);
            contact.information.put("city", adress.substring(adress.indexOf(adresssplit[1])).substring(5).trim());
        }

    }
}
